#include <math.h>
#include <progbase.h>
#include <progbase-cpp/console.h>
#include <thread>
#include <mutex>

#include <graphics.h>
#include <square.h>
#include <pentagon.h>

using namespace progbase::console;
using namespace std;

void clearSquare(Vec2D location, Vec2D size) {
	for (int y = 0; y < size.y; y++) {
		for (int x = 0; x < size.x; x++) {
			Vec2D pos = { x + location.x, y + location.y};
			Graphics_drawPixel(NULL, pos, CursorAttributes::BG_DEFAULT);
		}
	}
}

void drawSquare(Vec2D & center, double & edge, Color & color, mutex & m){
    Square square(center, edge, color);
	int degree = 0;
	while(1){
		square.rotate(degree++ * M_PI / 180.0);

		m.lock();
		clearSquare((Vec2D){0, 0}, (Vec2D){40, 25});
		square.draw();
		m.unlock();

		sleepMillis(20);
	}
}

void drawPentagon(Vec2D & center, Color & color, mutex & m){
    Pentagon pent;
    double period = 2;
    const int amp = 5;
    double radius = 7;
    int degrees = 0;
    double r;
	while(1){
        if(0 == period)
            period = 1;
        double radians = degrees * period * M_PI / 180.0;
        r = amp * sinf(radians);

		m.lock();
		clearSquare((Vec2D){40, 0}, (Vec2D){40, 25});
        pent.create(center, radius+r, color);
		m.unlock();

		sleepMillis(20);
        degrees++;
	}
}

int main(void){
	pb::conHideCursor();

	mutex m;

	Vec2D squareCenter = (Vec2D){20, 13};
    Vec2D pentagonCenter = (Vec2D){60, 13};

	double squareEdge = 10;

	Color squareColor = CursorAttributes::BG_YELLOW;
	Color pentagonColor = CursorAttributes::BG_BLUE;

	thread square(drawSquare, ref(squareCenter), ref(squareEdge), ref(squareColor), ref(m));
	thread pentagon(drawPentagon, ref(pentagonCenter), ref(pentagonColor), ref(m));

	while (!Console::isKeyDown());

	square.detach();
	pentagon.detach();

	sleepMillis(500);
	Console::setCursorAttribute(CursorAttributes::BG_DEFAULT);
	Console::clear();
	pb::conShowCursor();


	return 0;
}

