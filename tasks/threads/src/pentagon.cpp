#include "pentagon.h"
#include <math.h>

enum {
    edgeA = 1, edgeB = 2, edgeC = 3, edgeD = 4, edgeF = 5
};


void Pentagon::Pentagon_represent(Vec2D *vector, int r){
    vector[edgeA] = (Vec2D){vector[edgeA].x+r/10,vector[edgeA].y};
    vector[edgeD] = (Vec2D){(vector[edgeD].x+vector[edgeF].x)/2,vector[edgeD].y+ r/10};
    vector[edgeB] = (Vec2D){vector[edgeB].x- r/10,vector[edgeB].y};
}
void Pentagon::Pentagon_draw(Vec2D * vector, int color){
    int N = 6;
    int i = 0;
    for(i = 0; i < N-2; i++){
        Graphics_drawLine(NULL,vector[i],vector[i+1],color);
        if(i == 3){
            Graphics_drawLine(NULL,vector[i+1],vector[0],color);
            Graphics_drawLine(NULL,vector[i],vector[i+1],color);
        }
    }
}
void Pentagon::create(Vec2D center, int R, int color){
    int i, N = 6;
    double angle;
    Vec2D vector[N+1];
    for (i = 0; i < N; i++)
    {
        angle = 2*M_PI*i/N;
        vector[i] = (Vec2D){R*cos(angle)+center.x,R*sin(angle)+center.y};
    }
    Pentagon_represent(vector,R);
    Pentagon_draw(vector, color);


}