#include "square.h"
#include <math.h>

Square::Square(Vec2D center, double edge, Color color){
	this->center = center;
	this->edge = edge;
	this->color = color;
	
	this->originalVertex = (Vec2D){center.x + edge/2, center.y - edge/2};
	this->newVertex = this->originalVertex;
}

void Square::rotate(double radians){
    Vec2D vertexInCoordinatesOfCenter = Vec2D_toCenterCoords(originalVertex);
    Vec2D rotatedVertexInCenterCoords = Vec2D_rotate(vertexInCoordinatesOfCenter, radians);
    newVertex = Vec2D_toCoordisOfConsole(rotatedVertexInCenterCoords);
}

void Square::draw(){
	Vec2D vertexInCenterCoords = Vec2D_toCenterCoords(newVertex);

	double r = sqrt(pow(edge/2, 2) + pow(edge/2, 2));
	double nextAngle = M_PI - acos((2 * pow(r, 2) - pow(edge, 2)) / 2.0 / pow(r, 2));

	Vec2D vertexBInCenterCoords = Vec2D_rotate(vertexInCenterCoords, nextAngle);
	Vec2D vertexA = Vec2D_toCoordisOfConsole(vertexBInCenterCoords);

	Vec2D vertexCInCoordinatesOfCenter = Vec2D_rotate(vertexInCenterCoords, M_PI);
	Vec2D vertexB = Vec2D_toCoordisOfConsole(vertexCInCoordinatesOfCenter);

	Vec2D vertexDInCoordinatesOfCenter = Vec2D_rotate(vertexBInCenterCoords, M_PI);
	Vec2D vertexC = Vec2D_toCoordisOfConsole(vertexDInCoordinatesOfCenter);

	Graphics_drawLine(NULL, newVertex, vertexA, color);
	Graphics_drawLine(NULL, vertexC, newVertex, color);
    Graphics_drawLine(NULL, vertexA, vertexB, color);
    Graphics_drawLine(NULL, vertexB, vertexC, color);
}


Vec2D Square::Vec2D_toCoordisOfConsole(Vec2D vertex){
	return (Vec2D){vertex.x + center.x, vertex.y + center.y};
}

Vec2D Square::Vec2D_toCenterCoords(Vec2D vertex){
    return (Vec2D){vertex.x - center.x, vertex.y - center.y};
}