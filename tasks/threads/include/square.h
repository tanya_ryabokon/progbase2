#pragma once

#include <progbase.h>
#include <progbase-cpp/console.h>
#include "graphics.h"

typedef int Color;

class Square{
private:
	Vec2D center;
	Vec2D originalVertex;
	Vec2D newVertex;
	double edge;
	Color color;

	Vec2D Vec2D_toCenterCoords(Vec2D vertex);
	Vec2D Vec2D_toCoordisOfConsole(Vec2D vertex);
public:
	Square(Vec2D center, double edge, Color color);

	void draw();
	void rotate(double radians);
};