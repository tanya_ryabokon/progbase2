#pragma once

#include <progbase.h>
#include <progbase-cpp/console.h>

#include "graphics.h"

typedef int Color;

class Pentagon{
private:
	void Pentagon_represent(Vec2D *vector, int r);
	void Pentagon_draw(Vec2D * vector, int color);
public:
    void create(Vec2D center, int R, int color);
};