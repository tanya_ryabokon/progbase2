#include <stdio.h>
#include <stdlib.h>

typedef struct {
    char  	name[100];
    float	score;
} Student;

int length(int ld);
int integer_cmp(const void * a, const void * b);
void printArr(int * arr, int count);
void sort_integers(int arr[], int length);

int struct_cmp_by_score(const void *a, const void *b);
void print_struct_array(Student *array, size_t len);
void sort_structs(void);

void  action (int a, int b);
void Array_foreach(int arr[], int len, void (*action)(int, int));
void Array_foreachReversed(int arr[], int len, void (*action)(int, int));

int main(){
    int arr[] = {12, -345, 2, 44, -30, 2043, 3, 342, -23, 234234};
    int length = sizeof(arr) / sizeof (arr[0]);

    printf("1. Sort simple types\nSort array of integers\n\n");
    printf("Array: \n");
    sort_integers(arr, length);

    printf("\n2. Sort composite types\nSort array of Students by score\n\n");
    sort_structs();

    printf("\n3. Create callback functions\n");
    printf("Array:\n");
    printArr(arr, length);
    printf("\nArray foreach\n");
    Array_foreach(arr, length, action);
    printf("\nArray foreach reversed\n");
    Array_foreachReversed(arr, length, action);

    return 0;
}


int integer_cmp(const void * a, const void * b) {
    return length(*(int *)b)- length(*(int *)a);
}

void printArr(int * arr, int count) {
    for (int i = 0; i < count; i++) {
        printf("%i ", arr[i]);
    }
    puts("");
}
int length(int ld)
{
    return ld/10? 1+length(ld/10): 1;
}

void sort_integers(int arr[], int length)
{
    printArr(arr, length);
    printf("Sorted array: \n");
    qsort(arr, length, sizeof(arr[0]), integer_cmp);

    printArr(arr, length);
}

void print_struct_array(Student *array, size_t len)
{
    size_t i;

    for(i=0; i<len; i++)
        printf("[ name: %s \t score: %.2f ]\n", array[i].name, array[i].score);

    puts("--");
}

int struct_cmp_by_score(const void *a, const void *b)
{
    Student* st1 = (Student*)a;
    Student* st2 = (Student*)b;
    return (int)(100.f*st2->score - 100.f*st1->score);
}

void sort_structs(void)
{
    Student studs[] = {{"Taras", 4.3}, {"Tanya", 4.6},
                              {"Ann", 5.0}, {"Alex", 3.9},
                              {"Lera", 4.2}, {"Kirill", 4.5 }};

    size_t len = sizeof(studs) / sizeof(Student);

    puts("*** Struct sorting (score) ***");
    print_struct_array(studs, len);

    qsort(studs, len, sizeof(Student), struct_cmp_by_score);

    print_struct_array(studs, len);
}

void  action (int a, int b)
{
    if ( b > 0 && b % 2 != 0)
    printf("[ index: %i, value: %i ]\n", a, b);
}

void Array_foreach(int arr[], int len, void (*action)(int, int))
{
    for (int i = 0; i < len; i++)
    {
        action(i, arr[i]);
    }
}

void Array_foreachReversed(int arr[], int len, void (*action)(int, int))
{
    for (int i = len-1; i >= 0; i--)
    {
        action(i, arr[i]);
    }
}
