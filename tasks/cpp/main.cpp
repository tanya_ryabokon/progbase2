#include <iostream>
#include <vector>
#include "rectangle.h"
#include <cstdlib>

using namespace std;

void displayMenu();
void continueMenu();

int main() {
    std::vector<rectangle*> rectangles;
    int q = -1;

    while (1){
        system("clear");
        displayMenu();
        cin >> q;
        switch (q){
            case 1:
                if (rectangles.size() == 0){
                    cout <<  endl << "List is empty.\n" << endl;
                } else {
                    cout << endl << "All rectangles:\n" << endl;
                    for (int i = 0; i < rectangles.size(); i++)
                        rectangles[i]->print();
                }
                continueMenu();
            break;
            case 2: {
                string name = "";
                int a = 0;
                int b = 0;

                cout << endl << "Please, enter the name of rectangle:  ";
                cin >> name;
                cout << endl << "Please, enter the value of side A:  ";
                cin >> a;
                cout << endl << "Please, enter the value of side B:  ";
                cin >> b;
                rectangle *r = new rectangle(name, a, b);
                cout << endl << "Added element:\n" << endl;
                r->print();
                rectangles.push_back(r);
                continueMenu();
            }
                break;
            case 3: {
                long p = 0;

                cout << endl << "Please, enter P:  ";
                cin >> p;
                cout << endl << "All rectangles with perimeter greater than P:\n" << endl;
                for (int i = 0; i < rectangles.size(); i++)
                    if (rectangles[i]->calculatePerimeter() > p)
                        rectangles[i]->print();
                continueMenu();
            }
                break;
            case 4:
                for(int i = 0; i< rectangles.size(); i++)
                    delete rectangles[i];
                return 0;
        }

    }
}

void displayMenu(){
    cout << "1. Display the contents of the list in the console." << endl << \
           "2. Add new element to list." << endl << "3. Display all rectangles with perimeter greater than P." << endl \
            << "4. Exit." << endl << "Select: ";
}

void continueMenu() {
    cout << "Press Enter to continue." << endl;
    cin.ignore(1, ' ');
    cin.get();
}