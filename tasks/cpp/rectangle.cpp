//
// Created by tanya on 25.03.17.
//

#include "rectangle.h"

rectangle:: rectangle(string name, int a, int b){
    this->name = name;
    this->a = a;
    this->b = b;
}



long rectangle::calculatePerimeter(){
    return  this->a * this->b;
}

void rectangle::print() {
    cout << "[ " << this->name << " ; Side A = " << this->a << " ; Side B = " << this->b << " ; Perimeter = " << \
    calculatePerimeter() << " ]\n"  << endl;
}