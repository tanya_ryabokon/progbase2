//
// Created by tanya on 25.03.17.
//

#ifndef CPP_RECTANGLE_H
#define CPP_RECTANGLE_H

#include <iostream>

using namespace std;

class rectangle {
private:
    string name;
    int a;
    int b;
public:
    rectangle(string name, int a, int b);
    long calculatePerimeter();
    void print();
};


#endif //CPP_RECTANGLE_H
