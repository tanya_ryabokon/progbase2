#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <progbase/net.h>

#define BUFFER_LEN 1024

int main(void) {
    char storage[BUFFER_LEN*10] = "";
    //
    // create UDP client
    UdpClient * client = UdpClient_init(&(UdpClient){});
    IpAddress * serverAddress = IpAddress_init(&(IpAddress){}, "127.0.0.1", 9999);

    while (1) {
        NetMessage * message = NetMessage_init(
                &(NetMessage){},  // value on stack
                (char[BUFFER_LEN]){},  // array on stack
                BUFFER_LEN);
        puts("Please, input request:");
        puts("('allfiles' - to display files of current directory,\n'file <filename>' - to display the content of the file)");
        // @todo Read user input and create request message
        char request[100] = "";
        fgets(request, 100, stdin);
        NetMessage_setDataString(message, request);

        //
        // send string to server
        printf("\nSend string `%s` to server %s:%d\n",
               NetMessage_data(message),
               IpAddress_address(serverAddress),
               IpAddress_port(serverAddress));
        //
        // send request to server
        UdpClient_sendTo(client, message, serverAddress);
        while(0 != strcmp(NetMessage_data(message), "")) {

        //
        // blocking call to receive response data from server
            UdpClient_receiveFrom(client, message, serverAddress);
            strcat(storage,  NetMessage_data(message));
        }
        printf("\nReceived message from server: \n\n%s\n\n",
               storage);
        strcpy(storage, "");
    }
    //
    // close client
    UdpClient_close(client);
    return 0;
}