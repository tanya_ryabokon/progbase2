#include <stdio.h>
#include <dirent.h>
#include <stdlib.h>
#include <string.h>
#include <progbase/net.h>

#define BUFFER_LEN 1024
#define BUFFER_STR 100

typedef struct ClientRequest {
    char action[BUFFER_STR];
    char filename[BUFFER_STR];
} ClientRequest;

ClientRequest parseRequest(const char * msgStr);
void allFiles(char* list);


int main(void) {
    //
    // create UDP server
    UdpClient * server = UdpClient_init(&(UdpClient){});
    IpAddress * address = IpAddress_initAny(&(IpAddress){}, 9999);
    if (!UdpClient_bind(server, address)) {
        perror("Can't start server");
        return 1;
    }
    printf("Udp server started on port %d\n",
           IpAddress_port(UdpClient_address(server)));

    NetMessage * message = NetMessage_init(
            &(NetMessage){},  // value on stack
            (char[BUFFER_LEN]){},  // array on stack
            BUFFER_LEN);

    IpAddress clientAddress;
    while (1) {
        puts("Waiting for data...");
        //
        // blocking call to receive data from clients
        UdpClient_receiveFrom(server, message, &clientAddress);
        printf("Received message from %s:%d (%d bytes): `%s`\n",
               IpAddress_address(&clientAddress),  // client IP-address
               IpAddress_port(&clientAddress),  // client port
               NetMessage_dataLength(message),
               NetMessage_data(message));

        // @todo Process clients input and send response
        ClientRequest req = parseRequest(NetMessage_data(message));
        if (0 == strcmp(req.action, "allfiles")) {
            char data[BUFFER_LEN] = "";
            allFiles(data);
            NetMessage_setDataString(message, data);
            UdpClient_sendTo(server, message, &clientAddress);
        } else  if (0 == strcmp(req.action, "file")){

            char buffer[BUFFER_LEN] = "";
            FILE * fin = fopen(req.filename, "r");
            if (fin == NULL)
            {
                sprintf(buffer, "Error opening file %s\n", req.filename);
                NetMessage_setDataString(message, buffer);
                UdpClient_sendTo(server, message, &clientAddress);
            } else {
                fgets(buffer, BUFFER_LEN - 1, fin);
                while (!feof(fin)) {
                    fseek(fin, strlen(buffer), SEEK_CUR);
                    NetMessage_setDataString(message, buffer);
                    UdpClient_sendTo(server, message, &clientAddress);
                    fgets(buffer, BUFFER_LEN - 1, fin);
                }
                fclose(fin);
            }
        } else {
            NetMessage_setDataString(message, "No such command.");
            UdpClient_sendTo(server, message, &clientAddress);
        }
        NetMessage_setDataString(message, "");
        UdpClient_sendTo(server, message, &clientAddress);
    }
    //
    // close server
    UdpClient_close(server);
    return 0;
}

ClientRequest parseRequest(const char * msgStr) {
    ClientRequest request = {
            .action = "",
            .filename = ""
    };

    sscanf(msgStr, "%s %s", request.action,  request.filename);

    return request;
}

void allFiles(char* list)
{
    DIR *d;
    struct dirent *dir;
    d = opendir(".");
    if (d)
    {
        strcat(list, "All files in current directory:\n");
        while ((dir = readdir(d)) != NULL)
        {
            strcat(list, dir->d_name);
            strcat(list, "\n");
        }

        closedir(d);
        list[strlen(list) + 1] = '\0';
    }
}
