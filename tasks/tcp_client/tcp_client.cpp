#include <iostream>
#include <progbase-cpp/net.h>
#include <vector>
#include <jansson.h>
#include <string.h>

using namespace std;
using namespace progbase::net;


#define BUFFER_LEN 10000


void printIntVector(vector<int> vec);
void printStrVector(vector<string> vec);
void parseMessage(std::string &message,  vector<string>* data, char* error);
void modifyVector(vector<string> data, vector<int>* intData);

int main(void) {
    TcpClient client;
    NetMessage message(BUFFER_LEN);
    vector<string> data;
    vector<int> intData;
    char error[BUFFER_LEN];

    try {
        client.connect(IpAddress("127.0.0.1", 3000));
        message.setDataString("strnums");
        client.send(message);
        cout << ">> Request sent" << endl;
        client.receive(message);
        std::string str = message.dataAsString();
        cout << endl << ">> Received " << str.length() << " bytes: " << endl << str << endl;
        parseMessage(str, &data, error);
        cout << endl << ">> Received  vector: ";
        printStrVector(data);
    } catch(NetException const & exc) {
        cerr << exc.what() << endl;
    }
    modifyVector(data, &intData);
    cout << endl << ">>Task: If the string contains a number - the result will be that number, otherwise the result will be length of string.";

    if (error[0] == '\0')
        printIntVector(intData);
    else
        cout << endl << error << endl;


    return 0;
}

void parseMessage(std::string &message,  vector<string>* data, char* error) {
    size_t index = 0;
    char * writable = new char[message.size() + 1];
    std::copy(message.begin(), message.end(), writable);
    writable[message.size()] = '\0';
    json_error_t err;
    json_t *jsonArr = json_loads(writable, 0, &err);
    json_t *status = json_object_get(jsonArr, "status");
    if (json_boolean_value(status)) {
        json_t *dataJ = json_object_get(jsonArr, "data");
        json_t *elem;
        char buf [BUFFER_LEN];
        json_array_foreach(dataJ, index, elem) {
            strcpy(buf, (char *) json_string_value(elem));
            std::string str(buf);
            (*data).push_back(str);
        }
    } else {
        json_t *errorJ = json_object_get(jsonArr, "error");
        strcpy(error, ((char *) json_string_value(errorJ)));
    }
    json_decref(jsonArr);

}

void modifyVector(vector<string> data, vector<int>* intData)
{
    for (int i = 0; i < data.size(); i++) {
        std::string s = data[i];
        size_t digits = s.find_first_of( "1234567890+-" );
        if( digits <= s.size() ) {
            int z = atoi( s.c_str() + digits );
            (*intData).push_back(z);
        } else
            (*intData).push_back(s.length());

    }
}

void printIntVector(vector<int> vec)
{
    cout << endl;
    for (int i = 0; i < vec.size(); i++) {
        cout << vec[i] << " ";
    }
    cout << endl;
}

void printStrVector(vector<string> vec)
{
    cout << endl;
    for (int i = 0; i < vec.size(); i++) {
        cout << vec[i] << " ";
    }
    cout << endl;
}