#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "nlp.h"


enum {
    BUFFER_SIZE = 100
};

Text* readFromFile(const char * readFileName, char* text)
{
	char* buffer = malloc(2000 * sizeof(char));
	FILE * fin = fopen(readFileName, "r");
    text[0] = '\0';
	if (fin == NULL)
    {
        printf("Error opening file %s\n", readFileName);
        return NULL;
    }
	while (!feof(fin))
    {
		fgets(buffer, BUFFER_SIZE, fin);
		strcat(text, buffer);
		buffer[0] = '\0';
	}
    free(buffer);
    fclose(fin);
    return createText(text);
}

int writeToFile(const char * writeFileName, Text* text)
{
	 FILE * fout = fopen(writeFileName, "w");
    char str[10000];
	if (fout == NULL)
    {
        printf("Error opening file %s\n", writeFileName);
        return 1;
    }
    fputs(printText(text, str), fout);
	fclose(fout);
	return 0;
}

char** readStopWords(char* stW[], int * size)
{
    char* readFileName = "/home/tanya/projects/progbase2/tasks/nlp/stopwords.txt";
	FILE * fin = fopen(readFileName, "r");
	if (fin == NULL)
    {
        printf("Error opening file %s\n", readFileName);
        return stW;
    }
	int i = 0;
	while (!feof(fin))
    {
		fgets(stW[i], BUFFER_SIZE,fin);
        stW[i][strlen(stW[i])-1] = '\0';
		i++;
	}
	*size = i;
	 fclose(fin);
	 return stW;
}


