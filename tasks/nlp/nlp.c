#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include "files.h"

enum {
    BUFFER_SIZE = 100
};

struct Word {
    struct Word* next;
    char word[BUFFER_SIZE];
};

struct Sentence {
    struct Sentence* next;
    Word * h;
};

struct Text {
    struct Text* next;
    Sentence * h;
};

Word* createWord(char* word)
{
	Word* newWord = (Word *)malloc(sizeof(Word));
	newWord->next = NULL;
	strcpy(newWord->word, word);
	return newWord;
}



Word* createWordList(char* sentence)
{
	Word* h = NULL;
	char* p = NULL;
 	for(p = strtok(sentence, " ,"); p; p = strtok(NULL, " ,"))
    {
		Word* node = createWord(p);
		h = addWord(h, node);
	}
	return h;
}

void freeWord(Word** self)
{
    assert(NULL != self);
    free(*self);
    *self = NULL;
}

Word* addWord(Word* head, Word* node)
{
    if (head == NULL) {
        head = node;
        return head;
    }
    Word * cur = head;
    while (cur->next != NULL) {
        cur = cur->next;
    }
    cur->next = node;
    return head;

}

Sentence* createSentence(char* sentence)
{
	Sentence* newSentence = (Sentence*)malloc(sizeof(Sentence));
	newSentence->next = NULL;
	newSentence->h = createWordList(sentence);
	return newSentence;
}

Sentence* createSentenceList(char* text)
{
	Sentence* head = NULL;
	char* sentence = NULL;
    char textCopy[strlen(text) + 1];
    strcpy(textCopy, text);
    sentence = strtok(textCopy, ".");
    char * nextSentencePtr = text;
    while (NULL != sentence)
    {
        nextSentencePtr = nextSentencePtr + strlen(sentence) + 1;
		Sentence* node = createSentence(sentence);
        head = addSentence(head, node);
        sentence = strtok(nextSentencePtr, ".");
	}
    free(sentence);
	return head;
}

Sentence* addSentence(Sentence* head, Sentence* node)
{
    if (head == NULL) {
        head = node;
        return head;
    }
    Sentence * cur = head;
    while (cur->next != NULL) {
        cur = cur->next;
    }
    cur->next = node;
    return head;

}

void freeSentence(Sentence** self)
{
    assert(NULL != self);
    Word * cur = (*self)->h;
    while(cur != NULL) {
        Word * next = cur->next;
        freeWord(&cur);
        cur = next;
    }
    free(*self);
}

Text* createText(char* text)
{
	Text* newText = (Text*)malloc(sizeof(Text));
	newText->h = createSentenceList(text);
	return newText;
}

void freeText(Text** self)
{
    assert(NULL != self);
    Sentence * cur = (*self)->h;
    while(cur != NULL) {
        Sentence * next = cur->next;
        freeSentence(&cur);
        cur = next;
    }
    free(*self);
}

void deleteStopWords_Sentence(Sentence* sentence)
{
    int size = 1000;
    int i = 0;
    char* stW[size];
    if(sentence == NULL)
        return ;
	for (int j = 0; j < size; ++j) {
		stW[j] = (char*)malloc(1000 * sizeof(char));
	}
	readStopWords(stW, &size);
	for ( i = 0; i < size; i++)
	{
        char* res = (char*)malloc(80 * sizeof(char));
        deleteWords(&sentence->h, stW[i]);
        deleteWords(&sentence->h, toUpperFirstLetter(stW[i], res));
        free(res);
	}
    for (int j = 0; j < 1000; ++j) {
        free(stW[j]);
    }
	return;
}

void deleteStopWords_Text(Text* text)
{
	Sentence* c = text->h;
	while( c != NULL)
	{
        deleteStopWords_Sentence(c);
		c = c->next;
	}
    free(c);
}

void deleteWords(Word **head_ref, char* key)
{
    Word* temp = *head_ref, *prev;
    while (temp != NULL && strcmp(temp->word, key) == 0)
    {
        *head_ref = temp->next;
        free(temp);
        temp = *head_ref;
    }
    while (temp != NULL)
    {
        while (temp != NULL && strcmp(temp->word, key) != 0)
        {
            prev = temp;
            temp = temp->next;
        }
        if (temp == NULL) return;
        prev->next = temp->next;
        free(temp);
        temp = prev->next;
    }
}

char* printText(Text* text, char* textStr)
{
    textStr[0] = '\0';
    Sentence* cSentence = text->h;
    while(cSentence != NULL)
    {
        Word* cWord = cSentence->h;
        while(cWord != NULL)
        {
            strcat(textStr, cWord->word);
            if(cWord->next != NULL)
            strcat(textStr, ", ");
            cWord = cWord->next;
        }
        strcat(textStr, ".\n");
        cSentence = cSentence->next;
    }
    return textStr;
}
char* toUpperFirstLetter(char* word, char* res)
{
    char str[BUFFER_SIZE];
    strcpy(str, word);
    str[0] = str[0] - 32;
    strcpy(res, str);
    return res;
}

