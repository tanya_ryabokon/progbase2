#ifndef NLP_H
#define NLP_H

#include <stdlib.h>

typedef struct Word Word;
typedef struct Sentence Sentence;
typedef struct Text Text;

Sentence* createSentence(char* sentence);
Sentence* createSentenceList(char* text);
void freeSentence(Sentence** self);
Sentence* addSentence(Sentence* head, Sentence* node);

Word* createWord(char* word);
Word* createWordList(char* sentence);
Word* addWord(Word* head, Word* node);
void freeWord(Word** self);

Text* createText(char* text);
void freeText(Text** self);


void deleteStopWords_Sentence(Sentence* sentence);
void deleteStopWords_Text(Text* text);
void deleteWords(Word **head_ref, char* key);
char* printText(Text* text, char* textStr);
char* toUpperFirstLetter(char* word, char* res);

#endif