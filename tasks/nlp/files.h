#ifndef FILES_H
#define FILES_H

#include "nlp.h"

Text* readFromFile(const char * readFileName, char* text);
int writeToFile(const char * writeFileName, Text* text);
char** readStopWords( char* stW[], int * size);

#endif