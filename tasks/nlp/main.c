#include <stdlib.h>
#include <stdio.h>

#include "files.h"
#include "nlp.h"

int main()
{
	char* list = NULL; 
	char* textStr = malloc(10000*sizeof(char));
    Text* text =  readFromFile("/home/tanya/projects/progbase2/tasks/nlp/input.txt", textStr);
    deleteStopWords_Text(text);
    writeToFile("/home/tanya/projects/progbase2/tasks/nlp/output.txt", text);
    freeText(&text);
    free(textStr);
    free(list);
	return 0;
}
