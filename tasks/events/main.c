#include <stdlib.h>
#include <stdio.h>
#include "events.h"
#include <string.h>
#include <ctype.h>

char* filename = "/home/tanya/projects/progbase2/tasks/events/text.txt";

/* custom constant event type ids*/
enum {
    HexFoundEventTypeId,
    OverflowNEventTypeId,
    OverflowKEventTypeId
};

/* event handler functions prototypes */
void UpdatePrintHandler_update(EventHandler * self, Event * event);
void HexFoundHandler_update(EventHandler * self, Event * event);
void readAllText(const char * filename, char * text);

typedef struct OverflowCounter {
    long maxN;
    long maxK;
    long count;
} OverflowCounter;


#define new(TYPE, VAL) ({ TYPE * ptr = malloc(sizeof(TYPE)); *ptr = VAL; ptr; })

int main(void) {

    // startup event system
    EventSystem_init();

    // add event handlers
    OverflowCounter * c1 = new(OverflowCounter, ((OverflowCounter){
        .count = 0,
        .maxN= 30,
        .maxK = 60
    }));

    OverflowCounter * c2 = new(OverflowCounter, ((OverflowCounter){
            .count = 0,
            .maxN= 20,
            .maxK = 40
    }));
    char* text = (char*)malloc(sizeof(char)* 1000);

    EventSystem_addHandler(EventHandler_new(text, free, UpdatePrintHandler_update));
    EventSystem_addHandler(EventHandler_new(c1, free, HexFoundHandler_update));
    EventSystem_addHandler(EventHandler_new(c2, free, HexFoundHandler_update));

    // start infinite event loop
    EventSystem_loop();
    // cleanup event system
    EventSystem_cleanup();
    free(text);
    return 0;
}

/* event handlers functions implementations */

void UpdatePrintHandler_update(EventHandler * self, Event * event) {
    switch (event->type) {
        case StartEventTypeId: {
            readAllText(filename, (char*)self->data);
            puts("");
            puts("<<<START>>>");
            puts("");
            break;
        }
        case  UpdateEventTypeId:{
            if (isxdigit(((char*)self->data)[0])) {
                char* c = new(char, ((char*)self->data)[0]);
                EventSystem_raiseEvent(Event_new(self, HexFoundEventTypeId, c, free));
                printf("Found hexadecimal number %c \n",((char*)self->data)[0]);
            }
            (char*)(self->data)++;
            if ( ((char*)self->data)[0] == '\0') {
                self->data = NULL;
                EventSystem_exit();
            }
            break;
        }
        case OverflowNEventTypeId: {
            printf("Sum of hex = %lx greater than N = %lx\n\n", ((OverflowCounter*)event->data)->count, ((OverflowCounter*)event->data)->maxN);
            break;
        }
        case OverflowKEventTypeId: {
            printf("Sum of hex = %lx greater than K = %lx\n\n",  ((OverflowCounter*)event->data)->count, ((OverflowCounter*)event->data)->maxK);
            break;
        }
        case ExitEventTypeId: {
                puts("");
                puts("<<<EXIT!>>>");
                puts("");
                break;
        }
        default:
            return;;
    }
}


void HexFoundHandler_update(EventHandler * self, Event * event) {
    if ( event->type == HexFoundEventTypeId) {
        char num [10] = "";
        strcpy(num, (char*)event->data);

        ((OverflowCounter *) self->data)->count += strtol(num, NULL, 16);
        long * count = new(long, ((OverflowCounter *) self->data)->count);

        OverflowCounter * data = new(OverflowCounter, ((OverflowCounter){
                .count = ((OverflowCounter *) self->data)->count,
                .maxN= ((OverflowCounter *) self->data)->maxN,
                .maxK = ((OverflowCounter *) self->data)->maxK
        }));
           if (((OverflowCounter *) self->data)->maxK < *count) {
                EventSystem_raiseEvent(Event_new(self, OverflowKEventTypeId, data, free));
               ((OverflowCounter *)self->data)->count = 0;
            }else if (((OverflowCounter *) self->data)->maxN < *count) {
               EventSystem_raiseEvent(Event_new(self, OverflowNEventTypeId, data, free));
           }
           free(count);
    }
}



void readAllText(const char * filename, char * text) {
    char line[100];

    FILE *fr = fopen(filename, "r");
    if (fr == NULL) return;
    while (fgets(line, 100, fr)) {
        strcat(text, line);
    }
    strcat(text, "\0");
    fclose(fr);
    return;
}