#include "patient.h"


Patient::Patient()
{
	this->_age = 0;
	this->_name = "";
	this->_surname = "";
	this->_weight = 0;
}

Patient::Patient(string str)
{
	std::stringstream s(str);
	s >> this->_name >> this->_surname >> this->_age >> this->_weight;
}

Patient::~Patient()
{
	//std::cout << "Student is destoyed!" << std::endl;
}

void Patient::set_name(string name) { this->_name = name; }
void Patient::set_surname(string surname) { this->_surname = surname; }
void Patient::set_age(int age) { this->_age = age; }
void Patient::set_weight(float weight) { this->_weight = weight; }


string Patient::name() { return this->_name; }
string Patient::surname() { return this->_surname; }
int Patient::age() { return this->_age; }
float Patient::weight() { return this->_weight; }


//void Patient::print() {
//    std::cout << _name << " | " << _surname << " | " << _age << std::endl;
//}
