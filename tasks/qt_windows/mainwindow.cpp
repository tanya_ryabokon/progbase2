#include <QDebug>
#include <QCloseEvent>
#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	readFromFile();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::readFromFile(){
	FILE *file =fopen("/home/tanya/projects/progbase2/tasks/qt_windows/f1.txt","r");
	char buf[100];
	if (file != NULL){
		fgets (buf, 100, file);
		while (!feof(file))
		{
			Patient *p = new Patient(buf);
			fgets (buf, 100, file);
			patients.push_back(p);
		}
		QString size =  QString::number(patients.size()-1);
		ui->label_bounds->setText(size);
	}else
		ui->label_message->setText("File not found.");
}

void MainWindow::on_lineEdit_index_textChanged(const QString &arg1)
{
	if(ui->label_bounds->text() != QString("0")){
		ui->label_message->setText("");
		 if(ui->lineEdit_index->text() != QString("")){
			 bool ok;
			 (ui->lineEdit_index->text()).toDouble(&ok);
			 if (ok){
				int i = arg1.toInt();
					if (i < patients.size()){
					QString name =  QString::fromStdString(patients[i]->name());
					QString surname =  QString::fromStdString(patients[i]->surname());
					QString age =  QString::number(patients[i]->age());
					QString weight =  QString::number(patients[i]->weight());
					ui->label_valueName->setText(name);
					ui->label_valueSurname->setText(surname);
					ui->label_valueAge->setText(age);
					ui->label_valueWeight->setText(weight);
				}else
				{
					ui->label_message->setText("Index out of bounds.");
				}
			 } else{
				 ui->label_message->setText("Please, enter a number.");
			 }
		}
	} else
		ui->label_message->setText("File is empty or not found.");
}

void MainWindow::closeEvent (QCloseEvent *event)
{
	// use this to cleanup allocated memory

	for (Patient * pt : patients) {
		delete pt;
	}
	event->accept();
}
