#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "patient.h"
#include <vector>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
	void on_lineEdit_index_textChanged(const QString &arg1);
	 void closeEvent (QCloseEvent *event);
private:
    Ui::MainWindow *ui;
	std::vector<Patient*> patients;
	void readFromFile();
};

#endif // MAINWINDOW_H
