#ifndef PATIENT_H
#define PATIENT_H

#include <iostream>
#include <sstream>

using namespace std;

class Patient
{
	string _name;
	string _surname;
	int _age;
	float _weight;
public:
	Patient();
	Patient(string str);
	~Patient();

	void set_name(string name);
	void set_surname(string surname);
	void set_age(int age);
	void set_weight(float weight);

	string name();
	string surname();
	int age();
	float weight();
};


#endif // PATIENT_H
