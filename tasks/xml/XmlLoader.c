#include "XmlLoader.h"
void XmlLoader_saveToString(char * str, Patient *pt)
{

    xmlDoc * doc = NULL;
    xmlNode * patientNode = NULL;
    char strBuf[100];
    int i = 0;
    doc = xmlNewDoc(BAD_CAST "1.0");

    // create xml tree
    // create one root element
    patientNode = xmlNewNode(NULL, BAD_CAST "patient");
    xmlDocSetRootElement(doc, patientNode);
    //xmlDocSetRootElement(doc, rootNode);

    // student child
    xmlNewProp(patientNode, BAD_CAST "name", BAD_CAST pt->name);
    xmlNewProp(patientNode,BAD_CAST "surname", BAD_CAST pt->surname);
    sprintf(strBuf, "%i", pt->age);  // copy number to string
    xmlNewChild(patientNode, NULL, BAD_CAST "age", BAD_CAST strBuf);
    sprintf(strBuf, "%.1f", pt->weight);  // copy number to string
    xmlNewChild(patientNode, NULL, BAD_CAST "weight", BAD_CAST strBuf);
    // create group element as student child
    patientNode = xmlNewChild(patientNode, NULL, BAD_CAST "hospitalizations", NULL);
    while (i < pt->count) {
        xmlNode * xmlPtr = xmlNewChild(patientNode, NULL, BAD_CAST "hospitalization", BAD_CAST pt->hosp[i]->diagnosis);
        xmlNewProp(xmlPtr, BAD_CAST "data", BAD_CAST pt->hosp[i]->data);
        i++;
    }
    // copy xml contents to char buffer
    xmlBuffer * bufferPtr = xmlBufferCreate();
    xmlNodeDump(bufferPtr, NULL, (xmlNode *)doc, 0, 1);
    sprintf(str, "%s", (const char *)bufferPtr->content);
    xmlBufferFree(bufferPtr);
    xmlFreeDoc(doc);
}

void XmlLoader_loadFromString(Patient * pt, const char * xmlStr) {
    xmlDoc *xDoc = xmlReadMemory(xmlStr, strlen(xmlStr), NULL, NULL, 0);
    if (NULL == xDoc) {
        printf("Error parsing xml");
        return;
    }
    xmlNode *xRootEl = xmlDocGetRootElement(xDoc);
    char * name = (char *)xmlGetProp(xRootEl, BAD_CAST "name");
    char * surname = (char *)xmlGetProp(xRootEl, BAD_CAST "surname");
    strcpy(pt->name,name);
    strcpy(pt->surname, surname);
    free(name);
    free(surname);
    xmlNode *xCur = xRootEl->children;
    while (NULL != xCur) {
        if (XML_ELEMENT_NODE == xCur->type) {
            char* contentCur = (char *) xmlNodeGetContent(xCur);
            if (xmlStrcmp(xCur->name, BAD_CAST "age") == 0) {
                pt->age = atoi(contentCur);
            } else if (xmlStrcmp(xCur->name, BAD_CAST "weight") == 0) {
                pt->weight = atof(contentCur);
            }
            free(contentCur);
            xmlNode *xJ = xCur->children;
            int count = 0;
                while (NULL != xJ) {
                if (XML_ELEMENT_NODE == xJ->type) {
                    char* content = (char *) xmlNodeGetContent(xJ);
                     strcpy(pt->hosp[count]->diagnosis, content);
                     free(content);
                    if (xmlStrcmp(xJ->name, BAD_CAST "hospitalization") == 0) {
                        char * data = (char *)xmlGetProp(xJ, BAD_CAST "data");
                        strcpy(pt->hosp[count]->data, data);
                        count++;
                        free(data);
                     }
                }
                xJ = xJ->next;
            }
            pt->count = count;
        }
        xCur = xCur->next;
    }
    xmlFreeDoc(xDoc);
    return;
}


