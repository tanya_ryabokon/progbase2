#ifndef XMLLOADER_H
#define XMLLOADER_H

#include "convert.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <libxml/parser.h>
#include <libxml/tree.h>

void XmlLoader_saveToString(char * str, Patient * entity);
void XmlLoader_loadFromString(Patient * pt, const char * xmlStr);


#endif