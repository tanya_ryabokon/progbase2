#include "test.h"

const char * inFile = "patients.xml";

START_TEST (xml_void_loadFromString)
{
    Patient* p = Patient_create();
    char text[1000] = "";
    char buf1[1000] = "";
    char buf2[1000] = "";
    readAllText(inFile, text);
    Patient* self = Patient_new("Taras", "Stelmach", 30, 78.6, 2);
    self->hosp[0] = Hosp_new("URI", "03.02.2015");
    self->hosp[1] = Hosp_new("Flu", "10.05.2015");
    XmlLoader_loadFromString(p, text);
    ck_assert_str_eq(Patient_toString(p, buf1), Patient_toString(self, buf2));
    Patient_free(&self);
    Patient_free(&p);
}
END_TEST

START_TEST (xml_void_saveToString)
    {
        Patient* p = Patient_create();
        char text[1000] = "";
        char buf[1000] = "";
        readAllText(inFile, text);
        Patient* self = Patient_new("Taras", "Stelmach", 30, 78.6, 2);
        self->hosp[0] = Hosp_new("URI", "03.02.2015");
        self->hosp[1] = Hosp_new("Flu", "10.05.2015");
        XmlLoader_saveToString(buf, self);
        ck_assert_str_eq(buf, text);
        Patient_free(&self);
        Patient_free(&p);
    }
END_TEST

Suite *test_suite() {
    Suite *s = suite_create("Convert");
    TCase *tc_sample;
    tc_sample = tcase_create("TestCase");
    tcase_add_test(tc_sample, xml_void_loadFromString);
    tcase_add_test(tc_sample, xml_void_saveToString);
    suite_add_tcase(s, tc_sample);
    return s;
}
int test() {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_VERBOSE);

    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}
