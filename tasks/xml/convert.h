#ifndef CONVERT_H
#define CONVERT_H

#include <stdlib.h>
#include <assert.h>
#include <stdbool.h>

enum {
    SIZE = 20
};

struct Hospitalization{
    char* data;
    char* diagnosis;
};
typedef struct Hospitalization Hospitalization;
struct Patient{
    char* name;
    char* surname;
    int age;
    double weight;
    Hospitalization* hosp[SIZE];
    int count;
};
typedef struct Patient Patient;

Patient* Patient_create(void);
Patient * Patient_new(char* name, char* surname, int age, double weight, int count);
Hospitalization* Hosp_new(char* diagnosis, char* data);
void Patient_free(Patient ** selfPtr);
Hospitalization* Hosp_create();
void Hosp_free(Hospitalization ** selfPtr);
bool readAllText(const char * filename, char * text);
char * Patient_toString(Patient * self, char * buffer);

#endif