#ifndef TEST_H
#define TEST_H

#include <check.h>
#include "convert.h"
#include "JsonLoader.h"


Suite *test_suite();
int test();

#endif
