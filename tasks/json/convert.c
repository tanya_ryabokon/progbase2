#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "convert.h"

bool readAllText(const char * filename, char * text) {
    char line[100];

    FILE * fr = fopen(filename, "r");
    if (fr == NULL) return false;
    while(fgets(line, 100, fr)) {
        strcat(text, line);
    }
    fclose(fr);
    return true;
}

Hospitalization* Hosp_create()
{
    Hospitalization * self = malloc(sizeof(Hospitalization));
    self->data = (char*) malloc(sizeof(char) * 100);
    self->diagnosis = (char*) malloc(sizeof(char) * 100);
    return self;
}

Hospitalization* Hosp_new(char* diagnosis, char* data)
{
    Hospitalization * self = Hosp_create();
    strcpy(self->data, data);
    strcpy(self->diagnosis, diagnosis);
    return self;
}
Patient* Patient_create(void)
{
    Patient* self = (Patient*)malloc(sizeof(Patient));
    self->name = (char*) malloc(sizeof(char) * 100);
    self->surname = (char*) malloc(sizeof(char) * 100);
    for (int i = 0; i< SIZE; i++)
    {
        self->hosp[i] = Hosp_create();
    }
    return self;
}

Patient * Patient_new(char* name, char* surname, int age, double weight, int count)
{
    Patient * self = Patient_create();
    strcpy(self->name, name);
    strcpy(self->surname, surname);
    self->age = age;
    self->weight =  weight;
    self->count = count;
    return self;
}

void Patient_free(Patient ** selfPtr){
    free((*selfPtr)->name);
    free((*selfPtr)->surname);
    for (int i = 0; i< SIZE; i++)
    {
        Hosp_free(&(*selfPtr)->hosp[i]);
    }
    free(*selfPtr);
    *selfPtr = NULL;
}

void Hosp_free(Hospitalization ** selfPtr){
    free((*selfPtr)->data);
    free((*selfPtr)->diagnosis);
    free(*selfPtr);
    *selfPtr = NULL;
}

char * Patient_toString(Patient * self, char * buffer) {
    char sbuf[80];
    sprintf(buffer, "%s %s %i %f",
            self->name,
            self->surname,
            self->age,
            self->weight);
    for (int i = 0; i < self->count; i++)
    {
        sprintf(sbuf, " %s %s", self->hosp[i]->diagnosis, self->hosp[i]->data);
        strcat(buffer, sbuf);
    }
    return buffer;
}