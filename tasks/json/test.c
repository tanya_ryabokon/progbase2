#include "test.h"

const char * inFile = "/home/tanya/projects/progbase2/tasks/json/patients.json";

START_TEST (json_void_loadFromString)
{
    Patient* p = Patient_create();
    char text[1000] = "";
    char buf1[1000] = "";
    char buf2[1000] = "";
    readAllText(inFile, text);
    Patient* self = Patient_new("Taras", "Stelmach", 30, 78.6, 2);
    self->hosp[0] = Hosp_new("URI", "03.02.2015");
    self->hosp[1] = Hosp_new("Flu", "10.05.2015");
    JsonLoader_loadFromString(p, text);
    ck_assert_str_eq(Patient_toString(p, buf1), Patient_toString(self, buf2));
    Patient_free(&self);
    Patient_free(&p);
}
END_TEST

START_TEST (json_void_saveToString)
{
    int index = 0;
    json_error_t err;
    char text[1000] = "";
    char buf[1000] = "";
    readAllText(inFile, text);
    Patient* self = Patient_new("Taras", "Stelmach", 30, 78.6, 2);
    self->hosp[0] = Hosp_new("URI", "03.02.2015");
    self->hosp[1] = Hosp_new("Flu", "10.05.2015");
    JsonLoader_saveToString(buf, self);
    json_t * jsonArr = json_loads(buf, 0, &err);
    json_t * hospObj = json_object_get(jsonArr, "hospitalizations");
    json_t *elem;
    ck_assert_str_eq((char *) json_string_value(json_object_get(jsonArr, "name")), self->name);
    ck_assert_str_eq((char *) json_string_value(json_object_get(jsonArr, "surname")), self->surname);
    ck_assert_int_eq((int) json_integer_value(json_object_get(jsonArr, "age")), self->age);
    ck_assert_double_eq((double)json_real_value(json_object_get(jsonArr, "weight")), self->weight);
    json_array_foreach(hospObj, index, elem) {
        ck_assert_str_eq((char *) json_string_value(json_object_get(elem, "data")), self->hosp[index]->data);
        ck_assert_str_eq((char *) json_string_value(json_object_get(elem, "diagnosis")), self->hosp[index]->diagnosis);
    }
    json_decref(jsonArr);
    Patient_free(&self);
}
END_TEST

        Suite *test_suite() {
    Suite *s = suite_create("Convert");
    TCase *tc_sample;
    tc_sample = tcase_create("TestCase");
    tcase_add_test(tc_sample, json_void_loadFromString);
    tcase_add_test(tc_sample, json_void_saveToString);
    suite_add_tcase(s, tc_sample);
    return s;
}
int test() {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_VERBOSE);

    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}