#include "JsonLoader.h"

void JsonLoader_saveToString(char* str, const Patient * pt)
{
    json_t * json = json_object();
    json_object_set_new(json, "name", json_string(pt->name));
    json_object_set_new(json, "surname", json_string(pt->surname));
    json_object_set_new(json, "age", json_integer(pt->age));
    json_object_set_new(json, "weight", json_real(pt->weight));
    json_t * hospArr = json_array();
    for (int i = 0; i < pt->count; i++) {
        json_t * hospObj = json_object();
        json_object_set_new(hospObj, "data", json_string(pt->hosp[i]->data));
        json_object_set_new(hospObj, "diagnosis", json_string(pt->hosp[i]->diagnosis));
        json_array_append_new(hospArr, hospObj);
    }
    json_object_set_new(json, "hospitalizations", hospArr);
     strcpy(str, json_dumps(json, JSON_INDENT(2)));
    json_decref(json);
}

void JsonLoader_loadFromString(Patient * pt, const char * jsonString)
{
    json_error_t err;
    json_t * jsonArr = json_loads(jsonString, 0, &err);
    size_t index = 0;
    json_t *elem;
        json_t * hospObj = json_object_get(jsonArr, "hospitalizations");
        strcpy(pt->name, (char *) json_string_value(json_object_get(jsonArr, "name")));
        strcpy(pt->surname, (char *) json_string_value(json_object_get(jsonArr, "surname")));
        pt->age = (int) json_integer_value(json_object_get(jsonArr, "age"));
        pt->weight = json_real_value(json_object_get(jsonArr, "weight"));
        json_array_foreach(hospObj, index, elem) {
            strcpy(pt->hosp[index]->data , (char*) json_string_value(json_object_get(elem, "data")));
            strcpy(pt->hosp[index]->diagnosis ,(char *) json_string_value(json_object_get(elem, "diagnosis")));
            pt->count++;
        }
    json_decref(jsonArr);
    return;
}