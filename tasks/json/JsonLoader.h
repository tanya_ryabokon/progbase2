#ifndef JSONLOADER_H
#define JSONLOADER_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "convert.h"
#include <jansson.h>

void JsonLoader_saveToString(char * str, const Patient * pt);
void JsonLoader_loadFromString(Patient * pt, const char * jsonString);

#endif //JSONLOADER_H
