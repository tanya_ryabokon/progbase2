#ifndef FILE_H
#define FILE_H

#include "list.h"
#include "convert.h"

List * Storage_load(const char * fileName, List * inventors);
void Storage_save(const char * fileName, List * compositions);

#endif
