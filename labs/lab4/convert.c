#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <progbase.h>
#include <pbconsole.h>
#include "list.h"
#include "convert.h"
#include "cui.h"



Inventor * Inventor_new( char* name, int numOfProd, float productivity, char* invention, int year) {
    Inventor * self = malloc(sizeof(Inventor));
    strcpy(self->name, name);
    self->numberOfPatents = numOfProd;
    self->productivity = productivity;
    strcpy(self->p.invention, invention);
    self->p.year = year;
    return self;
}
Inventor * Inventor_create() {
    Inventor * self = malloc(sizeof(Inventor));
    strcpy(self->name, "");
    self->numberOfPatents = 0;
    self->productivity = 0;
    strcpy(self->p.invention, "");
    self->p.year = 0;
    return self;
}

void Inventor_free(Inventor ** selfPtr){
    free(*selfPtr);
    *selfPtr = NULL;
}

List* Inventor_rewrite(List * inventors,char* str, int index){
    Inventor* i = Inventor_newFromString(str);
    List_set(inventors, index, i);
    return inventors;
}

List* Inventor_rewriteField(List * inventors, int index, char* field , char* data){
    Inventor* i = List_get(inventors, index);
    if (strcmp(field, "name")==0)
    {
        strcpy(i->name, data);
    }else
    if (strcmp(field, "patents")==0)
    {
        int d = (int)strtod(data, NULL);
        i->numberOfPatents = d;
    }else
    if (strcmp(field, "productivity")==0)
    {
        float d = (float)strtod(data, NULL);
        i->productivity = d;
    }else
    if (strcmp(field, "invention")==0)
    {
        strcpy(i->p.invention, data);
    }else
    if (strcmp(field, "year")==0)
    {
        int d = (int)strtod(data, NULL);
        i->p.year = d;
    }
    else{
        noFieldPrint(field);
    }

    return inventors;
}



void Inventor_print(Inventor * self, int ind) {
    printf( "\n %i.  %s  %i  %.2f  %s  in  %i  year.\n",
            ind,
            self->name,
            self->numberOfPatents,
            self->productivity,
            self->p.invention,
            self->p.year);
}



char * Inventor_toString(Inventor * self, char * buffer) {
    sprintf(buffer, "%s %i %.2f %s in %i year.",
            self->name,
            self->numberOfPatents,
            self->productivity,
            self->p.invention,
            self->p.year);
    return buffer;
}

Inventor * Inventor_newFromString(char * str) {
    Inventor * self =  Inventor_create();
    if (strlen(str) == 0)
    {
        self = Inventor_create();
    } else {
        sscanf(str, "%[^0123456789\n] %d %f %[^0123456789\n] %i",
               self->name,
               &self->numberOfPatents,
               &self->productivity,
               self->p.invention,
               &self->p.year);
        self->name[strlen(self->name) - 1] = '\0';
        self->p.invention[strlen(self->p.invention) - 1] = '\0';
    }
    return self;
}

void Inventor_Find(List* list, int k)
{
    for ( int i = 0; i < List_count(list); i++){
        Inventor* inv = (Inventor*)List_get(list, i);
        if (inv->numberOfPatents < k){
            List_remove(list, inv);
            i--;
        }
    }
    return;
}


