#ifndef CUI_H
#define CUI_H

#include "list.h"

void displayMenu();
void addMenu();
void List_print(List * list);
void deleteMenu();
void rewriteMenu();
void rewriteFieldMenu();
void findKMenu();
void writeToFileMenu();
void clearInput(int q);
void noFieldPrint(char* field);

#endif