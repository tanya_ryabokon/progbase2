#include "file.h"
#include <stdio.h>

enum{
    BUFFER_SIZE = 1000
};

List * Storage_load(const char * fileName, List * inventors) {
    List_clear(inventors);
    char* buffer = malloc(2000 * sizeof(char));
    FILE * fin = fopen("/home/tanya/projects/progbase2/labs/lab4/inventors.txt", "r");
    if (fin == NULL)
    {
        printf("Error opening file %s\n", fileName);
        return NULL;
    }
    while (!feof(fin))
    {
        fgets(buffer, BUFFER_SIZE, fin);
        List_add(inventors, Inventor_newFromString(buffer));

    }
    free(buffer);
    fclose(fin);
    return inventors;
}

void Storage_save(const char * fileName, List * inventors) {

    FILE * fout = fopen(fileName, "w");
    char str[10000];
    str[0] = '\0';
    if (fout == NULL)
    {
        printf("Error opening file %s\n", fileName);
        return;
    }
    for (int i = 0; i < List_count(inventors); i++) {
        Inventor * c = (Inventor *)List_get(inventors, i);
        char * strPtr = Inventor_toString(c, str);
        fprintf(fout, "%s\n", strPtr);
    }
    fclose(fout);
    return;
}
