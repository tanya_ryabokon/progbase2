#include "tests.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <check.h>
#include "convert.h"

START_TEST (convert_fromStringToStruct_correctChanges)
    {
        Inventor* i = Inventor_newFromString("Benjamin Franklin 12 0.15 Lightning rod 1749");
        ck_assert_str_eq(i->name, "Benjamin Franklin");
        ck_assert_int_eq(i->numberOfPatents, 12);
        ck_assert_float_eq(i->productivity, 0.15);
        ck_assert_str_eq(i->p.invention, "Lightning rod");
        ck_assert_int_eq(i->p.year, 1749);
        Inventor_free(&i);
    }
END_TEST

START_TEST (convert_inventor_toString)
    {
        const char resultStr[] = "Nikola Tesla 300 3.50 Alternating current in 1889 year.";
        char buffer[80] = "";
        Inventor* i = Inventor_new("Nikola Tesla", 300, 3.5, "Alternating current", 1889);
        Inventor_toString(i, buffer);
        Inventor_free(&i);
        ck_assert_str_eq(buffer, resultStr);
    }
END_TEST


START_TEST (create_new_inventor)
    {
        Inventor* i = Inventor_new("Nikola Tesla", 300, 3.5, "Alternating current", 1889);
        ck_assert_str_eq(i->name, "Nikola Tesla");
        ck_assert_int_eq(i->numberOfPatents, 300);
        ck_assert_float_eq(i->productivity, 3.5);
        ck_assert_str_eq(i->p.invention, "Alternating current");
        ck_assert_int_eq(i->p.year, 1889);
        Inventor_free(&i);
    }
END_TEST

START_TEST (convert_inventor_emptystr)
    {
        Inventor* i = Inventor_newFromString("");
        ck_assert_str_eq(i->name, "");
        ck_assert_int_eq(i->numberOfPatents, 0);
        ck_assert_float_eq(i->productivity, 0);
        ck_assert_str_eq(i->p.invention, "");
        ck_assert_int_eq(i->p.year, 0);
        Inventor_free(&i);
    }
END_TEST

START_TEST (convert_inventor_changefield)
    {
        List* inv = List_new();
        List_add(inv, Inventor_new("Nikola Tesla", 300, 3.5, "Alternating current", 1889));
        Inventor_rewriteField(inv, 0, "patents", "200");
        Inventor_rewriteField(inv, 0, "invention", "Transformator");
        Inventor* i = List_get(inv, 0);
        ck_assert_str_eq(i->name, "Nikola Tesla");
        ck_assert_int_eq(i->numberOfPatents, 200);
        ck_assert_float_eq(i->productivity, 3.5);
        ck_assert_str_eq(i->p.invention, "Transformator");
        ck_assert_int_eq(i->p.year, 1889);
        Inventor_free(&i);
    }
END_TEST

START_TEST (convert_inventor_wrongfield)
    {
        List* inv = List_new();
        List_add(inv, Inventor_new("Nikola Tesla", 300, 3.5, "Alternating current", 1889));
        Inventor_rewriteField(inv, 0, "abcd", "200");
        printf("\n\n\n\n");
        Inventor* i = List_get(inv, 0);
        ck_assert_str_eq(i->name, "Nikola Tesla");
        ck_assert_int_eq(i->numberOfPatents, 300);
        ck_assert_float_eq(i->productivity, 3.5);
        ck_assert_str_eq(i->p.invention, "Alternating current");
        ck_assert_int_eq(i->p.year, 1889);
        Inventor_free(&i);
    }
END_TEST

START_TEST (convert_inventor_rewrite)
    {
        List* inv = List_new();
        List_add(inv, Inventor_new("Nikola Tesla", 300, 3.5, "Alternating current", 1889));
        Inventor_rewrite(inv, "Benjamin Franklin 12 0.15 Lightning rod 1749", 0);
        Inventor* i = List_get(inv, 0);
        ck_assert_str_eq(i->name, "Benjamin Franklin");
        ck_assert_int_eq(i->numberOfPatents, 12);
        ck_assert_float_eq(i->productivity, 0.15);
        ck_assert_str_eq(i->p.invention, "Lightning rod");
        ck_assert_int_eq(i->p.year, 1749);
        Inventor_free(&i);
    }
END_TEST

Suite *test_suite() {

    Suite *s = suite_create("Convert");

    TCase *tc_sample;

    tc_sample = tcase_create("TestCase");

    tcase_add_test(tc_sample, convert_fromStringToStruct_correctChanges);
    tcase_add_test(tc_sample, convert_inventor_toString);
    tcase_add_test(tc_sample, create_new_inventor);
    tcase_add_test(tc_sample, convert_inventor_changefield);
    tcase_add_test(tc_sample, convert_inventor_rewrite);
    tcase_add_test(tc_sample, convert_inventor_wrongfield);

    suite_add_tcase(s, tc_sample);

    return s;
}

int test() {
    Suite *s = test_suite();
    SRunner *sr = srunner_create(s);
    srunner_set_fork_status(sr, CK_NOFORK);

    srunner_run_all(sr, CK_VERBOSE);

    int num_tests_failed = srunner_ntests_failed(sr);
    srunner_free(sr);
    return num_tests_failed;
}
