#include "tcp_client.h"

tcp_client::tcp_client(char* path)
{
    try {
            auto ipAddress = IpAddress(Ip::resolveHostname("ghibliapi.herokuapp.com"), 443);
            client.connect(ipAddress, ssl);
            char httpWebRequest[256] = "";
            sprintf(httpWebRequest,"GET %s HTTP/1.0\r\n"
                                   "Host: ghibliapi.herokuapp.com\r\n"
                                   "\r\n", path);
            recievedMessage.setDataString(httpWebRequest);
            client.send(recievedMessage);
            cout << ">> Request sent" << endl;
            do {
                client.receive(recievedMessage);
                std::string str = recievedMessage.dataAsString();
                cout << ">> Received " << str.length() << " bytes: " << endl << str << endl;
                fullMessage = fullMessage + str;
            } while (!recievedMessage.isEmpty());
        } catch(NetException const & exc) {
            cerr << exc.what() << endl;
        }
}

string tcp_client::getMessage(){
    return fullMessage;
}
