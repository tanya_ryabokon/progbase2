#include "film.h"

film::film()
{
    this->id = "";
    this-> title = "";
    this->description="";
    this->director = "";
    this->producer = "";
    this->releaseDate = 0;
    this->rt_score = 0;
}

void film::setId(string _id){ this->id = _id; }
void film::setTitle(string _title){ this->title = _title; }
void film::setDescription(string _description) { this->description = _description; }
void film::setDirector(string _director) { this->director = _director; }
void film::setProducer(string _producer) { this->producer = _producer; }
void film::setReleaseDate(int _date) { this->releaseDate = _date; }
void film::setScore(int _score) { this->rt_score = _score; }
void film::addLocations(string _location) { this->locations = _location; }

string film::getId() { return this->id; }
string film::getTitle() { return this->title; }
string film::getDescription() { return this->description; }
string film::getDirector() { return this->director; }
string film::getProducer() { return this->producer; }
int film::getReleaseDate() { return this->releaseDate; }
int film::getScore() { return this->rt_score; }
string film::getLocations() { return this->locations; }


