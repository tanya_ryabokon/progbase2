#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "film.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void loadFilms(QString message);

private slots:
    void on_listWidget_films_currentItemChanged();

    void on_pushButton_find_clicked();

private:
    Ui::MainWindow *ui;
    std::vector<film*> films;

    std::vector<string> people_uri;
};

#endif // MAINWINDOW_H
