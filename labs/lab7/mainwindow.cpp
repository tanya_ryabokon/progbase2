#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "tcp_client.h"

using namespace std;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->lineEdit_find->setPlaceholderText("Type something...");
    this->setWindowTitle("Films");

    tcp_client client = tcp_client("/films");
    string str =  client.getMessage();
    string delimiter = "\r\n\r\n";
    size_t pos = str.find(delimiter);
    str.erase(str.begin(), str.begin() + pos + delimiter.size());
     QString qstr = QString::fromStdString(str);
    loadFilms(qstr);

    ui->comboBox->addItem("Director");
    ui->comboBox->addItem("Producer");
    ui->comboBox->addItem("Score ( >K )");
    ui->comboBox->addItem("Score ( <K )");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::loadFilms(QString message){
    QJsonDocument jdoc = QJsonDocument::fromJson(message.toUtf8());
    QJsonArray jarr = jdoc.array();
    foreach (const QJsonValue & value, jarr) {
        film* flm = new film();
        QJsonObject item = value.toObject();
            for (auto key : item.keys()) {
                auto p = item[key];

                if(key == "id"){
                    flm->setId(p.toString().toStdString());
                }
                else if(key == "title"){
                    flm->setTitle(p.toString().toStdString());
                }
                else if(key == "description"){
                    flm->setDescription(p.toString().toStdString());
                }
                else if(key == "director"){
                    flm->setDirector(p.toString().toStdString());
                }
                else if(key == "producer"){
                    flm->setProducer(p.toString().toStdString());
                }
                else if(key == "release_date"){
                    flm->setReleaseDate(p.toString().toInt());
                }
                else if(key == "rt_score"){
                    flm->setScore(p.toString().toInt());
                }
                else if(key == "people"){
                    QJsonArray ab = item[key].toArray();
                    string loc;
                    foreach (const QJsonValue & uri, ab) {
                        loc = loc + uri.toString().toStdString() + "\n";
                    }
                    flm->addLocations(loc);
                }

            }
            QListWidget * listWidget = this->findChild<QListWidget *>("listWidget_films");
            QString qstr = QString::fromStdString(flm->getTitle());
            listWidget->addItem(qstr);
            films.push_back(flm);
    }


}

void MainWindow::on_listWidget_films_currentItemChanged()
{
    QListWidget * listWidget = this->findChild<QListWidget *>("listWidget_films");
    int index = listWidget->currentIndex().row();

    film* flm = films[index];
    QString title =  QString::fromStdString(flm->getTitle());
    QString director =  QString::fromStdString(flm->getDirector());
    QString producer =  QString::fromStdString(flm->getProducer());
    QString date =  QString::number(flm->getReleaseDate());
    QString score =  QString::number(flm->getScore());
    QString description =  QString::fromStdString(flm->getDescription());
    QString loc = QString::fromStdString(flm->getLocations());

    ui->label_titleval->setText(title);
    ui->label_directorval->setText(director);
    ui->label_producerval->setText(producer);
    ui->label_dateval->setText(date);
    ui->label_scoreval->setText(score);
    ui->textEdit_description->setText(description);
    ui->textEdit_loc->setText(loc);
}



void MainWindow::on_pushButton_find_clicked()
{
    bool ok;
    ui->label_error->setText("");
    int val = ui->comboBox->currentIndex();
    QListWidget * listWidget = this->findChild<QListWidget *>("listWidget_find");
    listWidget->clear();
    QLineEdit* lineEdit = this->findChild<QLineEdit*>("lineEdit_find");
    QString value = lineEdit->text();
    switch (val){
        case 0:
            for (int i = 0; i < (int)films.size(); i++){
                if (((film*)films[i])->getDirector() == value.toStdString()){
                   QString qstr = QString::fromStdString(((film*)films[i])->getTitle());
                   listWidget->addItem(qstr);
                }
            }
        break;
        case 1:
            for (int i = 0; i < (int)films.size(); i++){
                if (((film*)films[i])->getProducer() == value.toStdString()){
                   QString qstr = QString::fromStdString(((film*)films[i])->getTitle());
                   listWidget->addItem(qstr);
                }
            }
        break;
        case 2:
            (ui->lineEdit_find->text()).toDouble(&ok);
            if(ok){
                for (int i = 0; i < (int)films.size(); i++){
                    if (((film*)films[i])->getScore() > value.toInt()){
                       QString qstr = QString::fromStdString(((film*)films[i])->getTitle());
                       listWidget->addItem(qstr);
                    }
                }
            } else{
                ui->label_error->setText("Please, enter a number.");
            }
        break;
        case 3:
            (ui->lineEdit_find->text()).toDouble(&ok);
            if(ok){
                for (int i = 0; i < (int)films.size(); i++){
                    if (((film*)films[i])->getScore() < value.toInt()){
                       QString qstr = QString::fromStdString(((film*)films[i])->getTitle());
                       listWidget->addItem(qstr);
                    }
                }
            } else{
                ui->label_error->setText("Please, enter a number.");
            }
        break;
    }
}
