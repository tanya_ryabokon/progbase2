#ifndef TCP_CLIENT_H
#define TCP_CLIENT_H

#include <iostream>
#include <progbase-cpp/net.h>

using namespace std;
using namespace progbase::net;

class tcp_client
{
public:
    tcp_client(char* path);
    Ssl ssl;
    TcpClient client;
    NetMessage recievedMessage = 101;
    string fullMessage = "";

    string getMessage();
};

#endif // TCP_CLIENT_H
