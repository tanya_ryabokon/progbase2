#ifndef FILM_H
#define FILM_H

#include <iostream>
#include<vector>
using namespace std;

class film
{
    string id;
    string title;
    string description;
    string director;
    string producer;
    int releaseDate;
    int rt_score;
    string locations;

public:
    film();

    void setId(string _id);
    void setTitle(string _title);
    void setDescription(string _description);
    void setDirector(string _director);
    void setProducer(string _producer);
    void setReleaseDate(int _date);
    void setScore(int _score);
    void addPeople(string _person);
    void addLocations(string _location);

    string getId();
    string getTitle();
    string getDescription();
    string getDirector();
    string getProducer();
    int getReleaseDate();
    int getScore();
    string getLocations();

};

#endif // FILM_H
