#ifndef CONVERT_H
#define CONVERT_H

#include <stdlib.h>
#include <assert.h>
#include "list.h"

typedef struct Inventor
{
    char name[100];
    int numberOfPatents;
    float productivity;
    struct patent
    {
        char invention[100];
        int year;
    } p;
    int index;
}  Inventor;

Inventor * Inventor_new(char* name, int numOfProd, float productivity, char* invention, int year);
Inventor * Inventor_create(void);
void Inventor_free(Inventor ** selfPtr);

void Inventor_print(Inventor * self, int ind);
List* Inventor_rewrite(List * inventors,char* str, int index);
List* Inventor_rewriteField(List * inventors, int index, char* field , char* data);

char * Inventor_toString(Inventor * self, char * buffer);
Inventor * Inventor_newFromString(char * str);
void Inventor_Find(List* list, int k);

#endif