#include "file.h"
#include <stdio.h>
#include <string.h>


enum{
    BUFFER_SIZE = 1000
};

List * Storage_load(const char * fileName, List * inventors) {
    List_clear(inventors);
    char* buffer = malloc(2000 * sizeof(char));
    FILE * fin = fopen(fileName, "r");
    if (fin == NULL)
    {
        printf("Error opening file %s\n", fileName);
        return NULL;
    }
    while (!feof(fin))
    {
        fgets(buffer, BUFFER_SIZE, fin);
        List_add(inventors, Inventor_newFromString(buffer));

    }
    free(buffer);
    fclose(fin);
    return inventors;
}

void Storage_save(const char * fileName, List * inventors) {

    FILE * fout = fopen(fileName, "w");
    char str[10000];
    str[0] = '\0';
    if (fout == NULL)
    {
        printf("Error opening file %s\n", fileName);
        return;
    }
    for (int i = 0; i < List_count(inventors); i++) {
        Inventor * c = (Inventor *)List_get(inventors, i);
        char * strPtr = Inventor_toString(c, str);
        fprintf(fout, "%s\n", strPtr);
    }
    fclose(fout);
    return;
}

void Storage_writeAsXml(const char * filePath, List * list) {
    xmlDoc * doc = NULL;
    xmlNode * inventorNode = NULL;
    xmlNode * patentNode = NULL;
    xmlNode * rootNode = NULL;
    char strBuf[100];
    char str[10000];
    doc = xmlNewDoc(BAD_CAST "1.0");

    FILE * fout = fopen(filePath, "w");
    str[0] = '\0';
    if (fout == NULL)
    {
        printf("Error opening file %s\n", filePath);
        return;
    }

    // create xml tree
    // create one root element
    rootNode = xmlNewNode(NULL, BAD_CAST "inventors");
    xmlDocSetRootElement(doc, rootNode);
    for (int i = 0; i < List_count(list); i++) {
        Inventor * inv = (Inventor *)List_get(list, i);
        // inventor child
        inventorNode = xmlNewChild(rootNode, NULL, BAD_CAST "inventor",NULL);
        xmlNewProp(inventorNode, BAD_CAST "fullname", BAD_CAST inv->name);
        sprintf(strBuf, "%i", inv->numberOfPatents);  // copy number to string
        xmlNewChild(inventorNode, NULL, BAD_CAST "numberOfPatents", BAD_CAST strBuf);
        sprintf(strBuf, "%.1f", inv->productivity);  // copy number to string
        xmlNewChild(inventorNode, NULL, BAD_CAST "productivity", BAD_CAST strBuf);
        // create group element as student child
        patentNode = xmlNewChild(inventorNode, NULL, BAD_CAST "patent",  inv->p.invention);
        sprintf(strBuf, "%i", inv->p.year);
        xmlNewProp(patentNode, BAD_CAST "year", BAD_CAST strBuf);
    }
    // copy xml contents to char buffer
    xmlBuffer * bufferPtr = xmlBufferCreate();
    xmlNodeDump(bufferPtr, NULL, (xmlNode *)doc, 0, 1);
    fprintf(fout, "%s", (const char *)bufferPtr->content);
    fclose(fout);
    xmlBufferFree(bufferPtr);
    xmlFreeDoc(doc);
}

List * Storage_readAsXml(const char * filePath, List * inventors) {
    List_clear(inventors);
    int count = 0;
    char text[10000] = "";
    if (!readAllText(filePath, text, 10000)) {
        printf("Error reading %s\n", filePath);
        return NULL;
    }

    xmlDoc *xDoc = xmlReadMemory(text, strlen(text), NULL, NULL, 0);
    if (NULL == xDoc) {
        printf("Error parsing xml");
        return NULL;
    }

    xmlNode *xRootEl = xmlDocGetRootElement(xDoc);
    for (xmlNode *xCur = xRootEl->children; NULL != xCur; xCur = xCur->next) {
        Inventor* inv = Inventor_create();
        if (XML_ELEMENT_NODE == xCur->type) {

            if (xmlStrcmp(xCur->name, BAD_CAST "inventor") == 0) {
                xmlNode *xInv = xCur;
                strcpy(inv->name, (char *) xmlGetProp(xInv, BAD_CAST "fullname"));
            }

            for (xmlNode *xJ = xCur->children; NULL != xJ; xJ = xJ->next) {

                if (XML_ELEMENT_NODE == xJ->type) {
                    xmlNode *xPatent = xJ;
                    if (xmlStrcmp(xJ->name, BAD_CAST "numberOfPatents") == 0) {
                        inv->numberOfPatents = atoi(xmlNodeGetContent(xJ));
                    } else
                    if (xmlStrcmp(xJ->name, BAD_CAST "productivity") == 0) {
                        inv->productivity = atof(xmlNodeGetContent(xJ));
                    }
                    if (xmlStrcmp(xJ->name, BAD_CAST "patent") == 0) {
                        strcpy(inv->p.invention,(char *)xmlNodeGetContent(xJ));
                        inv->p.year = atoi(xmlGetProp(xPatent, BAD_CAST "year"));
                    }
                }
            }
            inv->index = count;
            List_add(inventors, inv);
            count++;
        }
       // Inventor_free(&inv);
    }
    xmlFreeDoc(xDoc);
    return inventors;
}

void Storage_writeAsJson(const char * filePath, List * list) {
    FILE * fout = fopen(filePath, "w");
    if (fout == NULL)
    {
        printf("Error opening file %s\n", filePath);
        return;
    }
    json_t * array = json_array();
    for (int i = 0; i < List_count(list); i++) {
        Inventor *inv = (Inventor *) List_get(list, i);
        json_t *json = json_object();
        json_object_set_new(json, "fullname", json_string(inv->name));
        json_object_set_new(json, "numberOfPatents", json_integer(inv->numberOfPatents));
        json_object_set_new(json, "productivity", json_real(inv->productivity));
        json_t *patentObj = json_object();
        json_object_set_new(patentObj, "invention", json_string(inv->p.invention));
        json_object_set_new(patentObj, "year", json_integer(inv->p.year));
        json_object_set_new(json, "patent", patentObj);
        json_array_append_new(array, json);
    }
    // create JSON document string
    char * jsonString = json_dumps(array, JSON_INDENT(2));
    fputs(jsonString, fout);
    fclose(fout);
    free(jsonString);

    // decrease reference count (free's memory when count is 0')
    json_decref(array);
}

List * Storage_readAsJson(const char * filePath, List* inventors){
    List_clear(inventors);
    char jsonStr[10000] = "";
    FILE * fin = fopen(filePath, "r");
    if (fin == NULL)
    {
        printf("Error opening file %s\n", filePath);
        return NULL;
    }
    readAllText(filePath, jsonStr, 10000);
    int count = 0;
    json_error_t err;
    json_t * jsonArr = json_loads(jsonStr, 0, &err);
    int index = 0;
    json_t * value = NULL;
    json_array_foreach(jsonArr, index, value) {
        Inventor* inv = Inventor_create();
        json_t * patentObj = json_object_get(value, "patent");
        strcpy(inv->name,(char *)json_string_value(json_object_get(value, "fullname")));
        inv->numberOfPatents = (int) json_integer_value(json_object_get(value, "numberOfPatents"));
        inv->productivity = (float) json_real_value(json_object_get(value, "productivity"));
        strcpy(inv->p.invention, (char *)json_string_value(json_object_get(patentObj, "invention")));
        inv->p.year = (int) json_integer_value(json_object_get(patentObj, "year"));
        inv->index = count;
        List_add(inventors, inv);
        count++;
    }
    return  inventors;
}

bool readAllText(const char * filename, char * text, int maxSize) {
    char line[100];
    memset(text, 0, maxSize);
    FILE * fr = fopen(filename, "r");
    if (fr == NULL) return false;
    while(fgets(line, 100, fr)) {
        strcat(text, line);
    }
    fclose(fr);
    return true;
}