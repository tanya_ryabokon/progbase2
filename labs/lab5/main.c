#include <cui.h>
#include <stdio.h>
#include <stdlib.h>
#include <pbconsole.h>
#include <progbase.h>
#include <list.h>
#include <convert.h>
#include <file.h>
#include <tests.h>

int main()
{
    int ind = 0;
    char  str[1000];
    //char* filename = "/home/tanya/projects/progbase2/labs/lab5/inventors.json";
    char filename[100];
    List * inventors = List_new();
    int q = 0;
    displayMenu();
    while(1)
    {
        scanf("%i", &q);
        switch (q)
        {

            case 1:
            {
                clearInput(q);
                List_print(inventors);
                addMenu();
                scanf("%[^\n]", str);
                List_add(inventors, Inventor_newFromString(str));
                displayMenu();
                List_print(inventors);
                break;
            }
            case 2:
            {
                clearInput(q);
                displayMenu();
                printf("\n Enter file name:  ");
                scanf("%s", filename);
                int g = selectFormat(filename);
                switch (g)
                {
                    case 0:
                        Storage_load(filename, inventors);
                        break;
                    case 1:
                        Storage_readAsXml(filename, inventors);
                        break;
                    case 2:
                        Storage_readAsJson(filename, inventors);
                        break;
                }
                displayMenu();
                List_print(inventors);
                conMove(10,10);
                break;
            }
            case 3:
            {
                clearInput(q);
                displayMenu();
                List_print(inventors);
                deleteMenu();
                scanf("%i", &ind);
                if (ind<= List_count(inventors) - 1)
                {
                    List_removeAt(inventors, ind);
                    displayMenu();
                    List_print(inventors);
                }
                else{
                    displayMenu();
                    List_print(inventors);
                    printf("\n Index out of range of array.");
                    conMove(10,10);
                }
                break;
            }
            case 4:
            {
                clearInput(q);
                displayMenu();
                List_print(inventors);
                rewriteMenu();
                scanf("%i", &ind);
                if (ind <= List_count(inventors) - 1)
                {
                    displayMenu();
                    List_print(inventors);
                    addMenu();
                    clearInput(ind);
                    scanf("%[^\n]", str);
                    Inventor_rewrite(inventors, str, ind);
                    displayMenu();
                    List_print(inventors);
                }else{
                    displayMenu();
                    List_print(inventors);
                    printf("\n Index out of range of array.");
                    conMove(10,10);
                }
                break;
            }
            case 5:
            {
                char field[80];
                char data[80];
                clearInput(q);
                displayMenu();
                List_print(inventors);
                rewriteMenu();
                scanf("%i", &ind);
                if (ind <= List_count(inventors) - 1)
                {
                    displayMenu();
                    List_print(inventors);
                    rewriteFieldMenu();
                    clearInput(ind);
                    scanf("%s %s", field, data);
                    Inventor_rewriteField(inventors, ind, field, data);
                    displayMenu();
                    List_print(inventors);
                }else{
                    displayMenu();
                    List_print(inventors);
                    printf("\n Index out of range of array.");
                    conMove(10,10);
                }
                break;
            }
            case 6:
            {
                int k = 0;
                clearInput(q);
                displayMenu();
                List_print(inventors);
                findKMenu();
                scanf("%i", &k);
                clearInput(k);
                Inventor_Find(inventors, k);
                displayMenu();
                List_print(inventors);
                break;
            }
            case 7:
            {
                char foutname[100]; //= "/home/tanya/projects/progbase2/labs/lab5/out.json";
                clearInput(q);
                displayMenu();
                List_print(inventors);
                writeToFileMenu();
                scanf("%s", foutname);
                int g = selectFormat(foutname);
                switch (g)
                {
                    case 0:
                        Storage_save(foutname, inventors);
                        break;
                    case 1:
                        Storage_writeAsXml(foutname, inventors);
                        break;
                    case 2:
                        Storage_writeAsJson(foutname, inventors);
                        break;
                }
                displayMenu();
                conMove(10,10);
                break;
            }
            case 8:
            {
                displayMenu();
                printf("\n\n\n\n");
                test();
                printf("\n All tests passed.");
                conMove(10,10);
                break;
            }
            case 0:
            {
                displayMenu();
                puts("\n");
                return EXIT_SUCCESS;
            }
            default:
            {
                displayMenu();
                printf("\n Command not found.");
                conMove(10,10);
            }
        }
    }
    return EXIT_SUCCESS;
}


