#include <stdio.h>
#include <progbase.h>
#include <pbconsole.h>
#include "list.h"
#include "convert.h"
#include <string.h>

void displayMenu()
{
    conClear();
    conMove(1,2);
    printf("1. Add data to array");
    conMove(2,2);
    printf("2. Read array of data from file");
    conMove(3,2);
    printf("3. Delete data from array");
    conMove(4,2);
    printf("4. Rewrite data in array.");
    conMove(5,2);
    printf("5. Rewrite field in a structure in array.");
    conMove(6,2);
    printf("6. Find all inventors who have more than K patents");
    conMove(7,2);
    printf("7. Save changes in a file");
    conMove(8,2);
    printf("8. Test functions");
    conMove(9,2);
    printf("0. Exit");
    conMove(10,2);
    printf("Select: ");
}
void addMenu()
{
    conMove(11,2);
    printf("Please, enter information about inventor (name, number of patents,");
    conMove(12,2);
    printf("productivity, name of famous invention and a year it was created): ");
    conMove(13,2);

}
void rewriteMenu()
{
    conMove(11,2);
    printf("Please, enter the index of the item to rewrite: ");
}
void findKMenu()
{
    conMove(11,2);
    printf("Please, enter integer K: ");
}
void writeToFileMenu()
{
    conMove(11,2);
    printf("Please, enter name of file: ");
}
void rewriteFieldMenu()
{
    conMove(11,2);
    printf("Please, enter field to change and data");
    conMove(12,2);
    printf("Fields: 'name', 'patents', 'productivity', 'invention', 'year'");
    conMove(13,2);
}
void deleteMenu()
{
    conMove(11,2);
    printf("Please, enter the index of the item to remove: ");
}

void noFieldPrint(char* field)
{
    conMove(11,2);
    printf("There isn't field '%s'", field);
    conMove(10,10);
}

void List_print(List * list) {
    conMove(15,2);
    printf("№  Name:   Patents:   Productivity:   Famous invention:  \n");
    printf("-----------------------------------------------------------------");
    conMove(18,2);
    int len = List_count(list);
    for(int i = 0; i < len; i++) {
        void * value = List_get(list, i);
        Inventor * iValue = (Inventor *)value;
        Inventor_print(iValue, i);
    }
    conMove(10,11);
}

void clearInput(int q)
{
    while((q = getchar()) != '\n' && q != EOF) { }
}

int selectFormat(char* filename)
{
    if (strstr (filename, "txt") != NULL)
        return 0;
    if (strstr (filename, "xml") != NULL)
        return 1;
    if (strstr (filename, "json") != NULL)
        return 2;
}
