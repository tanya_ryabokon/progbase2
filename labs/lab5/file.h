#ifndef FILE_H
#define FILE_H

#include "list.h"
#include "convert.h"
#include <libxml/parser.h>
#include <libxml/tree.h>
#include <jansson.h>

List * Storage_load(const char * fileName, List * inventors);
void Storage_save(const char * fileName, List * compositions);

void Storage_writeAsXml(const char * filePath, List * list);
List * Storage_readAsXml(const char * filePath, List * inventors);

void Storage_writeAsJson(const char * filePath, List * list);
List * Storage_readAsJson(const char * filePath, List* inventors);

bool readAllText(const char * filename, char * text, int maxSize);

#endif
