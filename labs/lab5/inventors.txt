Nikola Tesla 300 3.5 Alternating current 1889
Benjamin Franklin 12 0.15 Lightning rod 1749
Thomas Edison 2332 27.8 Light bulb 1879
Alexander Graham Bell 7 0.09 Telephone 1876
George Westinghouse, Jr. 32 0.5 Railway air brake 1868
Jerome H. Lemelson 606 8.17 Machine vision 1954